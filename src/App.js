import Sesion from './fragment/Sesion';
import { Routes, Route, useLocation, Navigate } from 'react-router-dom';
import Principal from './fragment/Principal';
import Inicio from './fragment/Inicio';
import RegistroAuto from './fragment/RegistroAuto';
import RegistrarAuto from './fragment/Crud';
import { thisSession } from './utilidades/Sessionutil';
import Crud from './fragment/Crud';
import PresentarTablaAuto from './fragment/PresentarTablaAuto';
function App() {

  const Middeware = ({ children }) => {
    const autenticado = thisSession();
    const location = useLocation();

    if (autenticado) {
      return children;

    } else {
      return <Navigate to='/sesion' state={location} />;
    }

  }
  const MiddewareSession = ({ children }) => {
    const autenticado = thisSession();
    const location = useLocation();

    if (autenticado) {
      return <Navigate to='/inicio' />;

    } else {
      return children;
    }

  }


  return (
    <div className="App">
      <Routes>
        <Route path='/sesion' element={<MiddewareSession><Sesion /></MiddewareSession>} />
        <Route path='/inicio' element={<Middeware><Inicio /></Middeware>} />
        <Route path='/registro' element={<RegistroAuto />} />
        <Route path='/auto' element={<Crud />} />
        <Route path='/' element={<MiddewareSession><Principal /></MiddewareSession>} />
      </Routes>
    </div>
  );
}
export default App;