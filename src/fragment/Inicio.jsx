import { useNavigate } from 'react-router';
import '../css/stylea.css';
import { AutosCant, Marcas } from '../hooks/Conexion';
import { deleteSesion, getToken } from '../utilidades/Sessionutil';
import Footer from './Footer';
import Header from "./Header";
import mensajes from '../utilidades/Mensajes';
import { useState } from 'react';
import PresentarTablaAuto from './PresentarTablaAuto';


const Inicio = () => {
    const navegation = useNavigate();
    const [nro, setNro] = useState(0);
    const [nroA, setNroA] = useState(0);
    Marcas(getToken()).then((info) => {
        if (info.error == true && info.messaje == 'Acceso denegado. Token a expirado') {
            deleteSesion();
            mensajes(info.mensajes);
            navegation("/sesion")
        } else {
            //setNro(info.data.length);
        }
    });
    //const autos = Autos(getToken());
    AutosCant(getToken()).then((info) => {
        if (info.error == true && info.messaje == 'Acceso denegado. Token a expirado') {
            deleteSesion();
            mensajes(info.mensajes);
            navegation("/sesion")
        } else {
            //setNroA(info.data.nro);
            //console.log(nroA)
        }
    });

    return (
        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">
                    <Header />
                    {/**DE aqui cuerpo */}
                    <div className='container-fluid'>
                        <div className="row">

                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-left-primary shadow h-100 py-2">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                    Nro de Marcas</div>
                                                <div className="h5 mb-0 font-weight-bold text-gray-800">{nro}</div>
                                            </div>
                                            <div className="col-auto">
                                                <i className="fas fa-calendar fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-left-success shadow h-100 py-2">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                    Autos en existencia</div>
                                                <div className="h5 mb-0 font-weight-bold text-gray-800">{nroA}</div>
                                            </div>
                                            <div className="col-auto">
                                                <i className="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
}

export default Inicio;