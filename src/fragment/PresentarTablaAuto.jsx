import '../css/stylea.css';
import DataTable from 'react-data-table-component';
import Footer from './Footer';
import Header from './Header';
import { Autos } from '../hooks/Conexion';
import { useNavigate } from 'react-router';
import { deleteSesion} from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
import { useState } from 'react';

const ListaAuto = () => {
    const navegation = useNavigate();
    const [data, setData] = useState([]);
    
    Autos().then((info) => {
        if (info.error == true && info.messaje == 'Acceso denegado. Token a expirado') {
            deleteSesion();
            mensajes(info.mensajes);
            navegation("/sesion")
        } else {
            
            setData(info.data);
            //data = info.data
            //console.log(info.data);
            //setNro(info.data.length);
        }
    });
}

//const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;

const columns = [
    {
        name: 'Año',
        selector: row => row.anio,
    },
    {
        name: 'Cilindraje',
        selector: row => row.cilindraje,
    },
    {
        name: 'Color',
        selector: row => row.color,
    },
    {
        name: 'Placa',
        selector: row => row.placa,
    },
    {
        name: 'Estado',
        selector: row => row.estado,
    },
    {
        name: 'Precio',
        selector: row => row.precio,
    },
    {
        name: 'Marca',
        selector: row => row.marca_nombre,
    },
    {
        name: 'Acciones',
        selector: row => row.acciones,
    },
];

const data = [

    {
        anio: '2022',
        cilindraje: '1600',
        color: 'BLANCO',
        placa: 'ALH-09876',
        estado: '1',
        precio: '35000',
        marca_nombre: 'HYNDAY',
        acciones: (
            <>
                <a href="#" className="view" title="View" data-toggle="tooltip" style={{ color: "#10ab80" }}><i className="material-icons">&#xE417;</i></a>
                <a href="#" className="edit" title="Edit" data-toggle="tooltip"><i className="material-icons">&#xE254;</i></a>
                <a href="#" className="delete" title="Delete" data-toggle="tooltip" style={{ color: "red" }}><i className="material-icons">&#xE872;</i></a>

            </>)
    },

]

function PresentarTablaAuto() {
    return (
        <DataTable
            columns={columns}
            data={data}
            expandableRows
            expandableRowsComponent={ListaAuto}
        />
    );
};

export default PresentarTablaAuto;