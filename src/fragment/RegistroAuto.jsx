import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { ObtenerColores } from '../hooks/Conexion';

const RegistroAuto = () => {
  const navegation = useNavigate();
  const [colores, setColores] = useState([]);
  ObtenerColores().then((info) => {
    if (info.error === false) {
      console.log(info.data);
      setColores(info.data);
    }
  });
  const [validated, setValidated] = useState(false);

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }

    setValidated(true);
  };

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-md-8">
          <div className="card">
            <div className="card-header">Registro</div>
            <div className="card-body">

              <form className="form-horizontal" method="post" action="#">

                <div className="form-group">
                  <label htmlFor="name" className="cols-sm-2 control-label">Año;</label>
                  <div className="cols-sm-10">
                    <div className="input-group">

                      <input type="text" className="form-control" name="ano" id="ano" placeholder="Año" />
                    </div>
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="email" className="cols-sm-2 control-label">Cilindraje</label>
                  <div className="cols-sm-10">
                    <div className="input-group">

                      <input type="text" className="form-control" name="cilindraje" id="cilindraje" placeholder="Cilindraje" />
                    </div>
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="username" className="cols-sm-2 control-label">Color</label>
                  <div className="cols-sm-10">
                    <div className="input-group">

                      <input type="text" className="form-control" name="color" id="color" placeholder="Color" />
                    </div>
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="password" className="cols-sm-2 control-label">Placa</label>
                  <div className="cols-sm-10">
                    <div className="input-group">

                      <input type="password" className="form-control" name="placa" id="placa" placeholder="Placa" />
                    </div>
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="confirm" className="cols-sm-2 control-label">Estado</label>
                  <div className="cols-sm-10">
                    <div className="input-group">

                      <input type="password" className="form-control" name="estado" id="estado" placeholder="Estado" />
                    </div>
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="confirm" className="cols-sm-2 control-label">Precio</label>
                  <div className="cols-sm-10">
                    <div className="input-group">

                      <input type="password" className="form-control" name="precio" id="precio" placeholder="Precio" />
                    </div>
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="confirm" className="cols-sm-2 control-label">Marca</label>
                  <div className="cols-sm-10">
                    <div className="input-group">

                      <input type="password" className="form-control" name="marca" id="marca" placeholder="Marca" />
                    </div>
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="confirm" className="cols-sm-2 control-label">Acciones</label>
                  <div className="cols-sm-10">
                    <div className="input-group">

                      <input type="password" className="form-control" name="acciones" id="acciones" placeholder="Acciones" />
                    </div>
                  </div>
                </div>
                <div className="form-group ">
                  <button type="submit" className="btn btn-primary btn-lg btn-block login-button">Registrar</button>
                </div>
                <div className="login-register">
                  <a href="index.php">Login</a>
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>
    </div>
  );
}

export default RegistroAuto;