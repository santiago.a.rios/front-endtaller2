const URL = "http://localhost/v1/index.php";
const URLN = "http://localhost:3006/api";
export const InicioSesion = async (data) => {
    const headers = {
        'Accept': 'application/json',
        "Content-Type": "application/json"
    };
    const datos = await (await fetch(URLN + "/sesion", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    console.log(datos + " ");
    return datos;
}

export const Marcas = async (key) => {
    const cabeceras = { "X-API-KEY": key };
    const datos = await (await fetch(URL + "/marca", {
        method: "GET",
        headers: cabeceras
    })).json();
    return datos;
}

export const AutosCant = async (key) => {
    var cabeceras = { "X-API-KEY": key };
    const datos = await (await fetch(URL + "/auto/contar", {
        method: "GET",
        headers: cabeceras
    })).json();
    return datos;
}


export const Autos = async () => {
    const datos = await (await fetch(URL + "/auto", {
        method: "GET",
    })).json();
    //console.log(datos);
    return datos;
}

export const ObtenerColores = async () => {
    const datos = await (await fetch(URL + "/auto/colores", {
        method: "GET",
    })).json();
    //console.log(datos);
    return datos;
}