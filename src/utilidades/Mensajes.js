import swal from 'sweetalert';
const mensajes = (texto, type='succes', title='OK!') => swal({
        title: title,
        text: texto,
        icon: 'OK',
        button: 'OK',
        timer: 3000,
        closeOnEsc: true
    });

 
export default mensajes;